Assurez d'avoir un ordinateur sous Windows ou Linux (Nous avons utilisés Windows)
-
Démarrer un IDLE Python avec les prérequis nécessaires comme indiqué dans le fichier requirements.txt
-
Ouvrir le fichier index.py
-
Exécuter le code avec votre IDLE.
-
Seulement deux fenêtres doivent s’afficher. Si ce n’est pas le cas, veuillez fermer les fenêtres ouvertes et réitérer l’opération précédente.
-
Garder la console en vue. Elle pourra vous être utile dans le jeu.