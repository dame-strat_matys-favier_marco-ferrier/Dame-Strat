import tkinter as tk



#Création d'une fenêtre racine

fenetre=tk.Tk()
fenetre.title("Jeu de dames Dame'Strat")  # Définition du titre de la fenêtre


canvas=tk.Canvas(fenetre, width=1500, height=800)
canvas.pack()


# DAMIER DAMIER DAMIER

ligne=0
col=50
x=0
y=50
for j in range(5) :
    ligne=0
    col=50
    x=x+100
    y=y+100
    for i in range(5) :           
        ligne=ligne+50
        col=col+50
        canvas.create_rectangle(ligne,x,col,y, fill="black")
        ligne=ligne+50
        col=col+50
        canvas.create_rectangle(ligne,x,col,y, fill="white")
        
ligne=0
col=50
x=50
y=100
for j in range(5) :
    ligne=0
    col=50
    x=x+100
    y=y+100
    for i in range(5):           
        ligne=ligne+50
        col=col+50
        canvas.create_rectangle(ligne,x,col,y, fill="white")
        ligne=ligne+50
        col=col+50
        canvas.create_rectangle(ligne,x,col,y, fill="black")
    

# Initialisation

pions=[]
position_pions=[]

taille_pion = 50
couleur_g="grey"
couleur_b="blue"
selected_pion = None     # Aucun pion sélectionné au départ
selected_pion_id = None  # Aucun ID de pion sélectionné au départ
joueur = 1               # Initialisation du joueur 1



def creer_pion(x,y,couleur) :
  """Creé un pion à la position (x,y) avec la couleur donnée"""
  pion=canvas.create_oval(
    x*taille_pion,
    y*taille_pion,
    (x+1)*taille_pion,
    (y+1)*taille_pion,
    fill=couleur,
                         )
  pions.append(pion)
  position_pions.append((x,y))
  return pion



# dictionnaire quui représente la disposition initiale des pions sur le damier, avec des chaînes de caractères "x-y" qui représentent les coordonnées de chaque case, et  des valeurs 'g' pour les pions gris, 'b' pour les pions bleus et ' ' pour les cases vides

disposition_initiale = {
    
    '1-2': 'g', '2-2': ' ', '3-2': 'g', '4-2': ' ', '5-2': 'g', '6-2': ' ', '7-2': 'g', '8-2': ' ', '9-2': 'g', '10-2': ' ',
    '1-3': ' ', '2-3': 'g', '3-3': ' ', '4-3': 'g', '5-3': ' ', '6-3': 'g', '7-3': ' ', '8-3': 'g', '9-3': ' ', '10-3': 'g',
    '1-4': 'g', '2-4': ' ', '3-4': 'g', '4-4': ' ', '5-4': 'g', '6-4': ' ', '7-4': 'g', '8-4': ' ', '9-4': 'g', '10-4': ' ',
    '1-5': ' ', '2-5': 'g', '3-5': ' ', '4-5': 'g', '5-5': ' ', '6-5': 'g', '7-5': ' ', '8-5': 'g', '9-5': ' ', '10-5': 'g',
    '1-6': ' ', '2-6': ' ', '3-6': ' ', '4-6': ' ', '5-6': ' ', '6-6': ' ', '7-6': ' ', '8-6': ' ', '9-6': ' ', '10-6': ' ',
    '1-7': ' ', '2-7': ' ', '3-7': ' ', '4-7': ' ', '5-7': ' ', '6-7': ' ', '7-7': ' ', '8-7': ' ', '9-7': ' ', '10-7': ' ',
    '1-8': 'b', '2-8': ' ', '3-8': 'b', '4-8': ' ', '5-8': 'b', '6-8': ' ', '7-8': 'b', '8-8': ' ', '9-8': 'b', '10-8': ' ',
    '1-9': ' ', '2-9': 'b', '3-9': ' ', '4-9': 'b', '5-9': ' ', '6-9': 'b', '7-9': ' ', '8-9': 'b', '9-9': ' ', '10-9': 'b',
    '1-10': 'b', '2-10': ' ', '3-10': 'b', '4-10': ' ', '5-10': 'b', '6-10': ' ', '7-10': 'b', '8-10': ' ', '9-10': 'b', '10-10': ' ',
    '1-11': ' ', '2-11': 'b', '3-11': ' ', '4-11': 'b', '5-11': ' ', '6-11': 'b', '7-11': ' ', '8-11': 'b', '9-11': ' ', '10-11': 'b',
                        }



# Fonction pour initialiser les pions à partir du dictionnaire de disposition initiale ci-dessus

def initialiser_pions(dictionnaire):
    global position_pions           
    pions = []
    for coord, valeur in dictionnaire.items():
        x, y = map(int, coord.split('-'))
        if valeur == 'g':
            pion = creer_pion(x, y, couleur_g)
            pions.append(pion)
        elif valeur == 'b':
            pion = creer_pion(x, y, couleur_b)
            pions.append(pion)
        position_pions.append((x, y))  # Ajout de la position du pion à position_pions
        pions.append(pion)  # Ajout du pion à la liste globale pions
    return pions


# Initialisation des pions à partir du dictionnaire 

pions = initialiser_pions(disposition_initiale)



# Variables pour stocker les informations sur le pion sélectionné

def selectionner_pion(event) :
    """Fonction qui permet de sélectionner un pion lorsque l'on clic sur une case du damier"""
    global selected_pion, selected_pion_id
    x, y = event.x, event.y
    crX = 100
    crY = 150
    case_x = (x - crX) // 50
    case_y = (y - crY) // 50
    start = (case_x * taille_pion, case_y * taille_pion)
    
    print("Case sélectionnée:", (case_x, case_y))
    
    if (case_x, case_y) in position_pions :        # Vérifie si la case sélectionnée contient un pion
        index = position_pions.index((case_x, case_y))
        selected_pion_id = pions[index]
        selected_pion = (case_x, case_y) 
        print("Pion sélectionné:", selected_pion)
    else:
        print("Aucun pion sélectionné ou index invalide pour la liste pions.")



def move_piece(board, start, end):
    """Vérifie et effectue le déplacement d'une pièce sur le plateau de dames
    board : dict , plateau de jeu représenté sous forme de dictionnaire
    start : tuple , coordonnées de départ de la pièce (ligne, colonne)
    end : tuple , coordonnées de destination de la pièce (ligne, colonne)
    Return :
    bool , True si le déplacement est valide et effectué avec succès 
           False sinon"""
    
    if start not in board or end not in board :           # Vérifie si les coordonnées de départ et d'arrivée sont valides
        print("Déplacement impossible, les coordonnées sont invalides !")
        return False

    if board[start] != 'X' or board[end] != ' ' :     # Vérifie si la case de départ contient une pièce et si la case de destination est vide
        print("Déplacement impossible !")
        return False

    if abs(end[0] - start[0]) != 1 or abs(end[1] - start[1]) != 1 :   # Vérifie si le déplacement est diagonal et d'une seule case
        print("Déplacement impossible")
        return False
    

    board[end] = board[start] # Ceci met à jour le plateau de dames avec le déplacement de la pièce
    board[start] = ' '        #  Ceci efface le pion de la case de départ

    return True





def saut_possible(start, end, position_pions, joueur) :
   
    if end in position_pions :  # Vérifie si la case de destination est vide
        return False

    
    intermediaire = ((start[0] + end[0]) // 2, (start[1] + end[1]) // 2)  # Ceci calcule les coordonnées de la case intermédiaire

    
    if intermediaire not in position_pions or position_pions[intermediaire] == joueur : # Vérifie si la case intermédiaire est occupée par un pion adverse
        return False

    
    if abs(end[0] - start[0]) != 2 or abs(end[1] - start[1]) != 2 :  # Vérifie si le déplacement est diagonal
        return False

    
    if end[0] < 0 or end[0] > 10 or end[1] < 0 or end[1] > 10 :  # Vérifie si la case de destination est dans les limites du damier
        return False

    return True


# Fonction qui permet le déplacement des pions ( fonction à l'origine du problème du code avec la fonction "selectionner_pion")

def deplacer_pion(event) :
    global selected_pion, selected_pion_id, joueur
    x, y = event.x, event.y
    crX = 50
    crY = 100
    case_x = (x - crX) // 50
    case_y = (y - crY) // 50
    end = (case_x, case_y)
    
    print("Case sélectionnée:", (case_x, case_y))  # Ceci ajoute une impression pour vérifier si la case de départ est bien sélectionnée
    end = (case_x, case_y)
    print("Pion sélectionné:", selected_pion)  # Ceci ajoute une impression dans la console pour vérifier si le pion est bien sélectionné
    print("Case de destination:", end)  # Ceci ajoute une impression dans la console pour vérifier la case de destination
    
    if selected_pion is not None:  # Vérification du déplacement autorisé
         
        if abs(selected_pion[0] - case_x) == 1 and abs(selected_pion[1] - case_y) == 1 :  # Vérification pour un déplacement diagonal
            
            if end not in position_pions:
                
                if joueur == 1 and case_y > selected_pion[1] :   # Vérification si le joueur 1 joue ses pions vers le bas
                    
                    canvas.move(selected_pion_id, (case_x - selected_pion[0]) * taille_pion, (case_y - selected_pion[1]) * taille_pion)
                    position_pions.remove(selected_pion)
                    position_pions.append(end)
                    selected_pion = end
                    joueur = 2    # Met à jour le joueur actif
                
                elif joueur == 2 and case_y < selected_pion[1] :   # Vérification si le joueur 2 joue ses pions vers le haut
                    
                    canvas.move(selected_pion_id, (case_x - selected_pion[0]) * taille_pion, (case_y - selected_pion[1]) * taille_pion)
                    position_pions.remove(selected_pion)
                    position_pions.append(end)
                    selected_pion = end
                    joueur = 1  # Met à jour le joueur actif
                
                else:
                    print("Vous ne pouvez pas vous déplacer dans cette direction.")
            else:
                print("La case de destination est occupée.")
        elif abs(selected_pion[0] - case_x) == 1 :
            print("Merci de vous déplacer en diagonale (actuellement, horizontal).")
        elif abs(selected_pion[1] - case_y) == 1 :
            print("Merci de vous déplacer en diagonale (actuellement, vertical).")
        elif abs(selected_pion[0] - case_x) == 0 and abs(selected_pion[1] - case_y) == 0 :
                print("Déplacement nul.")
        else:
            print("Déplacement trop grand.")   
    else:
        print("Aucun pion sélectionné.")




def case(event) :
   from maths import floor #(pt module math )
   return (floor(event.x /100), floor(event.y /100))   # vérifie qu'elle retourne bien à chaque fois la bonne case



# Deuxième fenêtre pour les qui nous informe sur les règles du jeu 

racine=tk.Tk()
racine.title("Règles du jeu")
label=tk.Label(racine, text="Bienvenue sur notre jeu de dames Dame'Strat.\n\nCommencer par choisir votre niveau !\n\n\nLes règles sont les suivantes :\nVous ne pouvez que vous déplacer en diagonale et seulement en avant.\nPour prendre les pions de votre adversaire, vous devez sauter le pion adverse.(Dans ce cas précis, reculer est autorisé)\nVous pouvez enchainer les coups quand il s'agit de prendre des pions à votre adversaire.\nQuand votre pion arrive de l'autre côté du plateau, vous obtenez une dame (vous pouvez donc parcourir l'ensemble du plateau avec cette dernière.\nLe dernier à avoir encore des pions sur le plateau GAGNE ! ")



# Fonction "quitter" permet fermet la fnêtre racine

def quitter() :                                                
  racine.destroy()
bouton=tk.Button(racine, text="Quitter", command=quitter)


label.place(x=10, y=10)      
bouton.place(x=100, y=100)   

label.grid(row=0, column=0)
bouton.grid(row=1, column=1)



# Première fonction pour coder une situation de jeu ( ce qui doit être améliorer dans le futur )

def action_bouton():
    global disposition_initiale
    canvas.delete(pions)
    disposition_initiale={}
    disposition_initiale = {
    '1-2': 'g', '2-2': ' ', '3-2': 'g', '4-2': ' ', '5-2': ' ', '6-2': ' ', '7-2': 'g', '8-2': ' ', '9-2': 'g', '10-2': ' ',
    '1-3': ' ', '2-3': 'g', '3-3': ' ', '4-3': 'g', '5-3': ' ', '6-3': ' ', '7-3': ' ', '8-3': 'g', '9-3': ' ', '10-3': 'g',
    '1-4': 'g', '2-4': ' ', '3-4': ' ', '4-4': ' ', '5-4': ' ', '6-4': ' ', '7-4': 'g', '8-4': ' ', '9-4': 'g', '10-4': ' ',
    '1-5': ' ', '2-5': 'g', '3-5': ' ', '4-5': 'g', '5-5': ' ', '6-5': ' ', '7-5': ' ', '8-5': 'g', '9-5': ' ', '10-5': 'g',
    '1-6': ' ', '2-6': ' ', '3-6': ' ', '4-6': ' ', '5-6': ' ', '6-6': ' ', '7-6': ' ', '8-6': ' ', '9-6': ' ', '10-6': ' ',
    '1-7': ' ', '2-7': ' ', '3-7': ' ', '4-7': ' ', '5-7': ' ', '6-7': ' ', '7-7': ' ', '8-7': ' ', '9-7': ' ', '10-7': ' ',
    '1-8': 'b', '2-8': ' ', '3-8': ' ', '4-8': ' ', '5-8': ' ', '6-8': ' ', '7-8': 'b', '8-8': ' ', '9-8': 'b', '10-8': ' ',
    '1-9': ' ', '2-9': 'b', '3-9': ' ', '4-9': ' ', '5-9': ' ', '6-9': ' ', '7-9': ' ', '8-9': 'b', '9-9': ' ', '10-9': 'b',
    '1-10': 'b', '2-10': ' ', '3-10': ' ', '4-10': ' ', '5-10': ' ', '6-10': ' ', '7-10': 'b', '8-10': ' ', '9-10': 'b', '10-10': ' ',
    '1-11': ' ', '2-11': 'b', '3-11': ' ', '4-11': ' ', '5-11': ' ', '6-11': ' ', '7-11': ' ', '8-11': 'b', '9-11': ' ', '10-11': 'b',
}
    
    
    for i in range(len(disposition_initiale)) :
        initialiser_pions(ligne)





# Lier la fonction deplacer_pion à l'événement de clic de la souris sur le canvas

canvas.bind("<Button-1>", selectionner_pion) 

canvas.bind("<Button-3>", deplacer_pion) 
    

# Affichage des fenêtres "racine" et "fenetre"

racine.mainloop()
fenetre.mainloop()    


#http://www.xavierdupre.fr/app/teachpyx/helpsphinx/c_gui/tkinter.html
#https://darchevillepatrick.info/python/python19.php
#https://docs.python.org/fr/3/library/tkinter.html#a-hello-world-program
#https://realpython.com/
#http://www.xavierdupre.fr/app/teachpyx/helpsphinx/notebooks/partie_dame.html
